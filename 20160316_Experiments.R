library(ggplot2)
library(plyr)
library(zoo)



TGHC <- read.csv("~/Dropbox/Papers/World interRAI/20160226_QIPoster/20160218_PressureUlcerQI.csv", header=TRUE)
TCLHIN <- read.csv("~/Dropbox/Papers/World interRAI/20160226_QIPoster/20160229_RegionQI.csv", header=TRUE)
ADL <- read.csv("~/Dropbox/Papers/World interRAI/20160226_QIPoster/20160229_ADL.csv", header=TRUE)

data <- subset(TGHC, QI_Ind_Code == "QI_PRU06")
data <- subset(TCLHIN, QI_Ind_Code == "QI_PRU09")
data <- subset(TCLHIN, QI_Ind_Code == "QI_PRU06")
TGHC <- subset(TGHC, facility == "TGHC")
#TCLHIN <- subset(TCLHIN,organization_id == 3507)

TCLHIN <- rename(TCLHIN, c("organization_id"="facility"))

data <- rbind(TCLHIN, TGHC)

data$year <- substr(data$fiscal_quarter_ax,0,4)
data$quarter <- substr(data$fiscal_quarter_ax,5,6)


ADL$year <- substr(ADL$ax_fq,0,4)
ADL$quarter <- substr(ADL$ax_fq,6,7)

attach(data)
data$quartersub[quarter == "Q1"] <- "-04-01"
data$quartersub[quarter == "Q2"] <- "-07-01"
data$quartersub[quarter == "Q3"] <- "-10-01"
data$quartersub[quarter == "Q4"] <- "-01-01"
detach(data)


attach(ADL)
ADL$quartersub[quarter == "Q1"] <- "-04-01"
ADL$quartersub[quarter == "Q2"] <- "-07-01"
ADL$quartersub[quarter == "Q3"] <- "-10-01"
ADL$quartersub[quarter == "Q4"] <- "-01-01"
detach(ADL)


data$date <- paste(data$year, data$quartersub, sep="")

data$yq <- as.yearqtr(data$date, format = "%Y-%m-%d")

data <- subset(data, yq >= "2013 Q1")

ADL$date <- paste(ADL$year, ADL$quartersub, sep="")

ADL$yq <- as.yearqtr(ADL$date, format = "%Y-%m-%d")

ADL <- subset(ADL, yq >= "2013 Q1")

ADL <- subset(ADL, yq < "2015 Q1")


ADL$lhin <- factor(ADL$lhin)



ggplot(data, aes(x=fiscal_quarter_ax, y=QI_Adjusted)) +
	geom_line() + 
	geom_point()+  
	theme(axis.text.x = element_text(angle = 45, hjust = 1)) +  
	geom_bar(data=ADL, aes(x=yq, y=mean))




data$panel <- "Quality Indicator"

ADL$panel <- "Mean ADL Hierarchy Scale"

d <- rbind.fill(data, ADL)






ggplot() +
geom_line(data=d, aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color=facility))  + 
geom_point(data=d, aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color=facility))  + 
facet_grid(panel~., scale="free") + 
geom_line(data=d, aes(x=as.factor(yq), y=mean, group=lhin, color=lhin)) +
geom_point(data=d, aes(x=as.factor(yq), y=mean, group=facility, color=lhin)) + last_plot( )+ ylim (0,0.1)


a <- ggplot() +
geom_line(data=subset(d, facility == "TGHC"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color=facility))  + 
geom_point(data=subset(d, facility == "TGHC"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color=facility)) + ylim(0,0.15) + theme(legend.position="none") + xlab("") + ylab("Unadjusted\n QI Rate")

b <-ggplot() +  
geom_bar(data=subset(d, lhin == "0"), aes(x=as.factor(yq), y=mean, fill=lhin), stat="identity", position="dodge", width=0.25) + theme(legend.position="none") + xlab("Date") + ylab("% of Patients\n Late Loss ADL")

gridExtra::grid.arrange(a,b, ncol=1)



ggplot() + geom_boxplot(data=data, aes(x=as.factor(yq), y=QI_Adjusted)) + geom_line(data=subset(data, facility == "TGHC"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color=facility)) + ylim(0,0.2) + geom_point(data=subset(data, facility == "TGHC"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color=facility)) + stat_smooth()+ theme(legend.position="none")+ xlab("Date") + ylab("Adjusted QI Rate") + ggtitle("Percentage of Patients whose Stage 2-4 Pressure Ulcer Worsened")





test <- subset(data, yq =="2014 Q1")



	ggplot(ADL, aes(x=mean, y=mean)) + geom_bar()
	#stat_smooth(method=loess) 

ggplot(data=subset(data, facility=="TGHC"), aes(x=fiscal_quarter_ax, y=QI_Adjusted, group=facility, color=facility)) +
  geom_line() +
  geom_point() +
  theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
  stat_smooth(method=loess)

ggplot(data) + geom_smooth(aes(x=fiscal_quarter_ax, y=QI_Adjusted, group=facility, color=facility)) + geom_line(data=subset(data, aes(x=as.factor(yq), y=QI_Adjusted))  



  
  
  
  
  xlab("Year") +
  ylab("Pearson Correlation Coefficient") +
  #geom_vline(aes(xintercept = which(levels(quarter) %in% '2013_1'))) + 
  scale_x_discrete(breaks = c("2005_1", '2006_1', '2007_1', '2008_1', '2009_1', '2010_1', '2011_1', '2012_1', '2013_1','2014_1',"2015_1"), labels=c("2005","2006","2007","2008","2009","2010","2011","2012","2013","2014","2015")) +
  scale_y_continuous(breaks=seq(-0.5,1,by=0.25), limits=c(-0.5,1))
