setwd("~/Dropbox/Papers/World interRAI/20160226_QIPoster")
library(ggplot2)
library(plyr)
library(zoo)
library(gridExtra)

data <- read.csv("~/Dropbox/Papers/World interRAI/20160226_QIPoster/20160218_PressureUlcerQI.csv", header=TRUE)
rollingdata <- read.csv("~/Dropbox/Papers/World interRAI/20160226_QIPoster/20160320_AdjustedRollingQI.csv", header=TRUE)
purs <- read.csv("~/Dropbox/Papers/World interRAI/20160226_QIPoster/20160317_PURS4Plus.csv", header=TRUE)
purs05 <- read.csv("~/Dropbox/Papers/World interRAI/20160226_QIPoster/20160318_PURS05.csv", header=TRUE)


#Calculate Fiscal Quater 
data$year <- substr(data$fiscal_quarter_ax,0,4) #Split year out of string
data$quarter <- substr(data$fiscal_quarter_ax,5,6) #Split quarter of of string

attach(data)
data$quartersub[quarter == "Q1"] <- "-01-01" #Make fake dates so that you can turn the variable into Year-QTR format
data$quartersub[quarter == "Q2"] <- "-04-01"
data$quartersub[quarter == "Q3"] <- "-07-01"
data$quartersub[quarter == "Q4"] <- "-10-01"
detach(data)

data$date <- paste(data$year, data$quartersub, sep="") #Concatenate with year

data$yq <- as.yearqtr(data$date, format = "%Y-%m-%d") #Format in year quater

subset(purs, X_TYPE_ == "11")

purs$year <- substr(purs$ax_fq,0,4) #Split year out of string
purs$quarter <- substr(purs$ax_fq,5,6) #Split quarter of of string

attach(purs)
purs$quartersub[quarter == "Q1"] <- "-01-01" #Make fake dates so that you can turn the variable into Year-QTR format
purs$quartersub[quarter == "Q2"] <- "-04-01"
purs$quartersub[quarter == "Q3"] <- "-07-01"
purs$quartersub[quarter == "Q4"] <- "-10-01"
detach(purs)
purs$facility <- "TGHC"

purs$date <- paste(purs$year, purs$quartersub, sep="") #Concatenate with year

purs$yq <- as.yearqtr(purs$date, format = "%Y-%m-%d") #Format in year quater

subset(purs05, X_TYPE_ == "11")

purs05$year <- substr(purs05$ax_fq,0,4) #Split year out of string
purs05$quarter <- substr(purs05$ax_fq,5,6) #Split quarter of of string

attach(purs05)
purs05$quartersub[quarter == "Q1"] <- "-01-01" #Make fake dates so that you can turn the variable into Year-QTR format
purs05$quartersub[quarter == "Q2"] <- "-04-01"
purs05$quartersub[quarter == "Q3"] <- "-07-01"
purs05$quartersub[quarter == "Q4"] <- "-10-01"
detach(purs05)

purs05$date <- paste(purs05$year, purs05$quartersub, sep="") #Concatenate with year

purs05$yq <- as.yearqtr(purs05$date, format = "%Y-%m-%d") #Format in year quater

rollingdata$year <- substr(rollingdata$fiscal_quarter_ax,0,4) #Split year out of string
rollingdata$quarter <- substr(rollingdata$fiscal_quarter_ax,5,6) #Split quarter of of string

attach(rollingdata)
rollingdata$quartersub[quarter == "Q1"] <- "-01-01" #Make fake dates so that you can turn the variable into Year-QTR format
rollingdata$quartersub[quarter == "Q2"] <- "-04-01"
rollingdata$quartersub[quarter == "Q3"] <- "-07-01"
rollingdata$quartersub[quarter == "Q4"] <- "-10-01"
detach(rollingdata)

rollingdata$date <- paste(rollingdata$year, rollingdata$quartersub, sep="") #Concatenate with year

rollingdata$yq <- as.yearqtr(rollingdata$date, format = "%Y-%m-%d") #Format in year quater


pursplot <-ggplot() +  
	geom_bar(data=subset(purs, yq >= "2013 Q1"), aes(x=as.factor(yq), y=FullPercent), stat="identity", position="dodge", width=0.5) + 
	theme(legend.position="bottom") +
	xlab("Fiscal Quarter") + 
	ylab("% of Patients") + 
	ggtitle("Pressure Ulcer Risk Scale (PURS) 4+ for QI Calculation Eligible Patients")

pursplot_05 <-ggplot() +  
	geom_bar(data=subset(purs05, yq >= "2013 Q1" & yq < "2015 Q2"), aes(x=as.factor(yq), y=ColPercent, group=as.factor(pur), fill=as.factor(pur)), stat="identity", position="dodge") + 
	theme(legend.position="bottom") +
	xlab("Fiscal Quarter") + 
	ylab("% of Patients") + 
	scale_fill_discrete(name="Scale Score", labels=c("0", "1-2", "3", "4-5", "6+")) + 
	ggtitle("Pressure Ulcer Risk Scale (PURS) 4+ for QI Calculation Eligible Patients")






#Plot for Worsen PU, Adjusted
PRU06A <- ggplot() +
	geom_boxplot(data=subset(rollingdata, QI_Ind_Code == "QI_PRU06" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted)) +
	geom_line(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU06" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="red"), size=1.5) +
	geom_point(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU06"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="red"), size=3) + 	
	geom_line(data=subset(rollingdata, facility == "TGHC" & QI_Ind_Code == "QI_PRU06" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="blue"), size=1.5) +
	geom_point(data=subset(rollingdata, facility == "TGHC" & QI_Ind_Code == "QI_PRU06"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="blue"), size=3) + 
	ylim(0,0.2) +
	theme(legend.position="none")+
	xlab("") + ylab("Adjusted QI Rate") +
	ggtitle("Percentage of Patients whose Stage 2-4 Pressure Ulcer Worsened,\n TGHC vs. Toronto Central LHIN CCC Facilities") +
	scale_colour_manual(name = '', values =c('red'='red','blue'='blue'), labels = c('TGHC Rolling 4 Quarter Average','TGHC Raw QI Rate')) + 
	theme(plot.title = element_text(size=22))

PRU06UN <- ggplot() +
	geom_line(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU06" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color="red"), size=1.5) +
	geom_point(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU06"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color="red"), size=3) + 	
	geom_line(data=subset(rollingdata, facility == "TGHC" & QI_Ind_Code == "QI_PRU06" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color="blue"), size=1.5) +
	geom_point(data=subset(rollingdata, facility == "TGHC" & QI_Ind_Code == "QI_PRU06"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color="blue"), size=3) + 
	ylim(0,0.2) +
	theme(legend.position="bottom")+
	xlab("") + ylab("Unadjusted QI Rate") +
	#ggtitle("Percentage of Patients whose Stage 2-4 Pressure Ulcer Worsened") + 
	scale_colour_manual(name = '', values =c('red'='red','blue'='blue'), labels = c('TGHC Rolling 4 Quarter Average','TGHC Raw QI Rate'))


	#pdf(file="20160316_QIPRU06.pdf", width=12, height=6)
	gridExtra::grid.arrange(PRU06A,PRU06UN,pursplot, ncol=1)
	#dev.off()
	
PRU09A <- ggplot() +
	geom_boxplot(data=subset(rollingdata, QI_Ind_Code == "QI_PRU09" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted)) +
	geom_line(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU09" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="red"), size=1.5) +
	geom_point(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU09"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="red"), size=3) + 	
	geom_line(data=subset(rollingdata, facility == "TGHC" & QI_Ind_Code == "QI_PRU09" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="blue"),size=1.5) +
	geom_point(data=subset(rollingdata, facility == "TGHC" & QI_Ind_Code == "QI_PRU09"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="blue"), size=4) + 
	ylim(0,0.2) +
	theme(legend.position="none")+
	xlab("") + ylab("Adjusted QI Rate") +
	ggtitle("Percentage of Patients with New Stage 2-4 Pressure Ulcer,\n TGHC vs. Toronto Central LHIN CCC Facilities") +
	scale_colour_manual(name = '', values =c('red'='red','blue'='blue'), labels = c('TGHC Rolling 4 Quarter Average','TGHC Raw QI Rate')) + 
	theme(plot.title = element_text(size=22))


PRU09UN <- ggplot() +
	geom_line(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU09" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color="red"), size=1.5) +
	geom_point(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU09"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color="red"), size=3) + 	
	geom_line(data=subset(rollingdata, facility == "TGHC" & QI_Ind_Code == "QI_PRU09" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color="blue"), size=1.5) +
	geom_point(data=subset(rollingdata, facility == "TGHC" & QI_Ind_Code == "QI_PRU09"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color="blue"), size=3) + 
	ylim(0,0.2) +
	theme(legend.position="bottom")+
	xlab("") + ylab("Unadjusted QI Rate") +
	#ggtitle("Percentage of Patients whose Stage 2-4 Pressure Ulcer Worsened") + 
	scale_colour_manual(name = '', values =c('red'='red','blue'='blue'), labels = c('TGHC Rolling 4 Quarter Average','TGHC Raw QI Rate'))


	#pdf(file="20160316_QIPRU09.pdf", width=12, height=6)
	gridExtra::grid.arrange(PRU09A,PRU09UN,pursplot, ncol=1)
	#dev.off()
	
















	
#Plot for New PU, Adjusted
PRU09A <- ggplot() +
	geom_boxplot(data=subset(rollingdata, QI_Ind_Code == "QI_PRU09" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted)) +
	geom_line(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU09" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="red")) +
	geom_point(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU09"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="red")) + 	
	geom_line(data=subset(rollingdata, facility == "TGHC" & QI_Ind_Code == "QI_PRU09" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="blue")) +
	geom_point(data=subset(rollingdata, facility == "TGHC" & QI_Ind_Code == "QI_PRU09"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="blue")) + 
	ylim(0,0.3) +
	theme(legend.position="bottom")+
	xlab("Fiscal Quarter") + ylab("Adjusted QI Rate") +
	ggtitle("Percentage of Patients with New Stage 2-4 Pressure Ulcer") + 
	scale_colour_manual(name = '', values =c('red'='red','blue'='blue'), labels = c('Rolling 4 Quarter Average','Raw QI Rate'))

	ggsave(file="20160316_QIPRU09A.pdf", plot=PRU09A, width=8.59, height=5.73)


#Plot for Worsen PU, Unadjusted
PRU06UN <- ggplot() +
	geom_line(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU06" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color="red")) +
	ylim(0,0.3) +
	geom_point(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU06"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color="red")) + 
	theme(legend.position="bottom")+
	ylab("Unadjusted QI Rate") + xlab ("") + 
	ggtitle("Percentage of Patients whose Stage 2-4 Pressure Ulcer Worsened") + 
	scale_colour_manual(name = '', values =c('red'='red'), labels = c('Raw QI Rate'))
	
	pdf(file="20160316_QIPRU06UN.pdf", width=8.59, height=5.73)
	gridExtra::grid.arrange(PRU06UN,pursplot, ncol=1)
	dev.off()



#Plot for New PU
PRU09UN<- ggplot() +
	geom_line(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU09"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color="red")) +
	ylim(0,0.3) +
	geom_point(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU09"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color="red")) + 
	theme(legend.position="bottom")+
	ylab("Unadjusted QI Rate") + xlab("") + 
	ggtitle("Percentage of Patients with New Stage 2-4 Pressure Ulcer") +
	scale_colour_manual(name = '', values =c('red'='red'), labels = c('Raw QI Rate'))

	pdf(file="20160316_QIPRU09UN.pdf", width=8.59, height=5.73)
	gridExtra::grid.arrange(PRU09UN,pursplot, ncol=1)
	dev.off()




#Plot for on Admit, unadjusted
PRU05UN <- ggplot() +
	geom_line(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU05" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color="red")) +
	ylim(0,0.3) +
	geom_point(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU05"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Unadjusted, group=facility, color="red")) + 
	theme(legend.position="bottom")+
	xlab("") + ylab("Unadjusted QI Rate") +
	ggtitle("Percentage of Patients with a Stage 2-4 Pressure Ulcer") + 
	scale_colour_manual(name = '', values =c('red'='red'), labels = c('Raw QI Rate'))

	pdf(file="20160316_QIPRU05UN.pdf", width=8.59, height=5.73)
	gridExtra::grid.arrange(PRU05UN,pursplot_05, ncol=1)
	dev.off()
	
#Plot for PU on admit, adjusted
PRU05A <- ggplot() +
	geom_boxplot(data=subset(rollingdata, QI_Ind_Code == "QI_PRU05" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted)) +
	geom_line(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU05" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="red")) +
	geom_point(data=subset(data, facility == "TGHC" & QI_Ind_Code == "QI_PRU05"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="red")) + 	
	geom_line(data=subset(rollingdata, facility == "TGHC" & QI_Ind_Code == "QI_PRU05" & yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="blue")) +
	geom_point(data=subset(rollingdata, facility == "TGHC" & QI_Ind_Code == "QI_PRU05"& yq >= "2013 Q1"), aes(x=as.factor(yq), y=QI_Adjusted, group=facility, color="blue")) + 
	ylim(0,0.3) +
	theme(legend.position="bottom")+
	xlab("Fiscal Quarter") + ylab("Adjusted QI Rate") +
	ggtitle("Percentage of Patients with a Stage 2-4 Pressure Ulcer") + 
	scale_colour_manual(name = '', values =c('red'='red','blue'='blue'), labels = c('Rolling 4 Quarter Average','Raw QI Rate'))


ggsave(file="20160316_QIPRU05A.pdf", width=8.59, height=5.73)

