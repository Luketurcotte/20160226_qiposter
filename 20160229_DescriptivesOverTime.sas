data lhin(compress=binary);
	set level5.ccrs_2015;
	if province_code='ON' and /*facility_type_code*/sector_code=3;
	if fac_health_region="Toronto Central";
	if facility_code_mbun ne 3854;
	*if facility_random in (2,6,7,10,11);
	* note, am hard-coding these ax_fq and aquart values to be consistent with data_aggregate_qi macro code assignments - JWP - Feb 2012;
	if '01oct2008'd le a3 le '31dec2008'd then do; ax_fq='2008Q3';aquart=50 ; end;
	else if '01jan2009'd le a3 le '31mar2009'd then do; ax_fq='2008Q4';aquart=51 ; end;
	else if '01apr2009'd le a3 le '30jun2009'd then do; ax_fq='2009Q1';aquart=52 ; end;
	else if '01jul2009'd le a3 le '30sep2009'd then do; ax_fq='2009Q2';aquart=53 ; end;
	else if '01oct2009'd le a3 le '31dec2009'd then do; ax_fq='2009Q3';aquart=54 ; end;
	else if '01jan2010'd le a3 le '31mar2010'd then do; ax_fq='2009Q4';aquart=55 ; end;
	else if '01apr2010'd le a3 le '30jun2010'd then do; ax_fq='2010Q1';aquart=56 ; end;
	else if '01jul2010'd le a3 le '30sep2010'd then do; ax_fq='2010Q2';aquart=57 ; end;
	else if '01oct2010'd le a3 le '31dec2010'd then do; ax_fq='2010Q3';aquart=58 ; end;
	else if '01jan2011'd le a3 le '31mar2011'd then do; ax_fq='2010Q4';aquart=59 ; end;
	else if '01apr2011'd le a3 le '30jun2011'd then do; ax_fq='2011Q1';aquart=60 ; end;
	else if '01jul2011'd le a3 le '30sep2011'd then do; ax_fq='2011Q2';aquart=61 ; end;
	else if '01oct2011'd le a3 le '31dec2011'd then do; ax_fq='2011Q3';aquart=62 ; end;
	else if '01jan2012'd le a3 le '31mar2012'd then do; ax_fq='2011Q4';aquart=63 ; end;
	else if '01apr2012'd le a3 le '30jun2012'd then do; ax_fq='2012Q1';aquart=64 ; end;
	else if '01jul2012'd le a3 le '30sep2012'd then do; ax_fq='2012Q2';aquart=65 ; end;
	else if '01oct2012'd le a3 le '31dec2012'd then do; ax_fq='2012Q3';aquart=66 ; end;
	else if '01jan2013'd le a3 le '31mar2013'd then do; ax_fq='2012Q4';aquart=67 ; end;
	else if '01apr2013'd le a3 le '30jun2013'd then do; ax_fq='2013Q1';aquart=68 ; end;
	else if '01jul2013'd le a3 le '30sep2013'd then do; ax_fq='2013Q2';aquart=69 ; end;
	else if '01oct2013'd le a3 le '31dec2013'd then do; ax_fq='2013Q3';aquart=70 ; end;
	else if '01jan2014'd le a3 le '31mar2014'd then do; ax_fq='2013Q4';aquart=71 ; end;
	else if '01apr2014'd le a3 le '30jun2014'd then do; ax_fq='2014Q1';aquart=72 ; end;
	else if '01jul2014'd le a3 le '30sep2014'd then do; ax_fq='2014Q2';aquart=73 ; end;
	else if '01oct2014'd le a3 le '31dec2014'd then do; ax_fq='2014Q3';aquart=74 ; end;
	else if '01jan2015'd le a3 le '31mar2015'd then do; ax_fq='2014Q4';aquart=75 ; end;
	else if '01apr2015'd le a3 le '30jun2015'd then do; ax_fq='2015Q1';aquart=76 ; end;
	else if '01jul2015'd le a3 le '30sep2015'd then do; ax_fq='2015Q2';aquart=77 ; end;

	else delete;
	run;

proc sort data=lhin;by aquart;run;

proc univariate data=lhin noprint;
	var adl_hier_nh2;
	by ax_fq;
	output out=adl mean=mean std=std;
	run;


proc univariate data=lhin noprint;
	var cps_nh2;
	by ax_fq;
	output out=cps mean=mean std=std;
	run;

proc univariate data=lhin noprint;
	var chess_nh2;
	by ax_fq;
	output out=chess mean=mean std=std;
	run;

proc univariate data=lhin noprint;
	var purs_nh2;
	by ax_fq;
	output out=purs mean=mean std=std;
	run;


data tghc(compress=binary);
	set level5.ccrs_2015;
	if province_code='ON' and /*facility_type_code*/sector_code=3;
	if fac_health_region="Toronto Central";
	if facility_code_mbun = 3854;
	*if facility_random in (2,6,7,10,11);
	* note, am hard-coding these ax_fq and aquart values to be consistent with data_aggregate_qi macro code assignments - JWP - Feb 2012;
	if '01oct2008'd le a3 le '31dec2008'd then do; ax_fq='2008Q3';aquart=50 ; end;
	else if '01jan2009'd le a3 le '31mar2009'd then do; ax_fq='2008Q4';aquart=51 ; end;
	else if '01apr2009'd le a3 le '30jun2009'd then do; ax_fq='2009Q1';aquart=52 ; end;
	else if '01jul2009'd le a3 le '30sep2009'd then do; ax_fq='2009Q2';aquart=53 ; end;
	else if '01oct2009'd le a3 le '31dec2009'd then do; ax_fq='2009Q3';aquart=54 ; end;
	else if '01jan2010'd le a3 le '31mar2010'd then do; ax_fq='2009Q4';aquart=55 ; end;
	else if '01apr2010'd le a3 le '30jun2010'd then do; ax_fq='2010Q1';aquart=56 ; end;
	else if '01jul2010'd le a3 le '30sep2010'd then do; ax_fq='2010Q2';aquart=57 ; end;
	else if '01oct2010'd le a3 le '31dec2010'd then do; ax_fq='2010Q3';aquart=58 ; end;
	else if '01jan2011'd le a3 le '31mar2011'd then do; ax_fq='2010Q4';aquart=59 ; end;
	else if '01apr2011'd le a3 le '30jun2011'd then do; ax_fq='2011Q1';aquart=60 ; end;
	else if '01jul2011'd le a3 le '30sep2011'd then do; ax_fq='2011Q2';aquart=61 ; end;
	else if '01oct2011'd le a3 le '31dec2011'd then do; ax_fq='2011Q3';aquart=62 ; end;
	else if '01jan2012'd le a3 le '31mar2012'd then do; ax_fq='2011Q4';aquart=63 ; end;
	else if '01apr2012'd le a3 le '30jun2012'd then do; ax_fq='2012Q1';aquart=64 ; end;
	else if '01jul2012'd le a3 le '30sep2012'd then do; ax_fq='2012Q2';aquart=65 ; end;
	else if '01oct2012'd le a3 le '31dec2012'd then do; ax_fq='2012Q3';aquart=66 ; end;
	else if '01jan2013'd le a3 le '31mar2013'd then do; ax_fq='2012Q4';aquart=67 ; end;
	else if '01apr2013'd le a3 le '30jun2013'd then do; ax_fq='2013Q1';aquart=68 ; end;
	else if '01jul2013'd le a3 le '30sep2013'd then do; ax_fq='2013Q2';aquart=69 ; end;
	else if '01oct2013'd le a3 le '31dec2013'd then do; ax_fq='2013Q3';aquart=70 ; end;
	else if '01jan2014'd le a3 le '31mar2014'd then do; ax_fq='2013Q4';aquart=71 ; end;
	else if '01apr2014'd le a3 le '30jun2014'd then do; ax_fq='2014Q1';aquart=72 ; end;
	else if '01jul2014'd le a3 le '30sep2014'd then do; ax_fq='2014Q2';aquart=73 ; end;
	else if '01oct2014'd le a3 le '31dec2014'd then do; ax_fq='2014Q3';aquart=74 ; end;
	else if '01jan2015'd le a3 le '31mar2015'd then do; ax_fq='2014Q4';aquart=75 ; end;
	else if '01apr2015'd le a3 le '30jun2015'd then do; ax_fq='2015Q1';aquart=76 ; end;
	else if '01jul2015'd le a3 le '30sep2015'd then do; ax_fq='2015Q2';aquart=77 ; end;

	else delete;
	run;

proc sort data=tghc;by aquart;run;

proc univariate data=tghc noprint;
	var adl_hier_nh2;
	by ax_fq;
	output out=adl_tghc mean=mean std=std;
	run;

proc univariate data=tghc noprint;
	var cps_nh2;
	by ax_fq;
	output out=cps_tghc mean=mean std=std;
	run;

proc univariate data=tghc noprint;
	var chess_nh2;
	by ax_fq;
	output out=chess_tghc mean=mean std=std;
	run;

proc univariate data=tghc noprint;
	var purs_nh2;
	by ax_fq;
	output out=purs_tghc mean=mean std=std;
	run;

data adl;set adl;lhin=1;run;
data cps;set cps;lhin=1;run;
data chess;set chess;lhin=1;run;
data purs;set purs;lhin=1;run;

data adl_tghc;set adl_tghc;lhin=0;run;
data cps_tghc;set cps_tghc;lhin=0;run;
data chess_tghc;set chess_tghc;lhin=0;run;
data purs_tghc;set pur_tghcs;lhin=0;run;

data adl_new;
set adl adl_tghc;
run;

data cps_new;
set cps cps_tghc;
run;

data chess_new;
set chess chess_tghc;
run;

data purs_new;
set purs purs_tghc;
run;





